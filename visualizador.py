import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import os, sys

directorioPymol = "/usr/bin/pymol/modules"
# Se setea el directorio de pymol
sys.path.insert(0,directorioPymol)
os.environ['PYMOL_PATH'] = os.path.join(directorioPymol, 'pymol/pymol_path')
import pymol
from pymol import cmd


def archivojson():
    try:
        with open('listamoleculas.json', 'r') as file:
            data = json.load(file)
        file.close()
    except IOError:
        data = []
    return data


def save_file(data):
	print("Save File")

	with open('listamoleculas.json', 'w') as file:
		json.dump(data, file, indent=2)
		file.close()


class Imagen():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")
		self.ventana0 = self.builder.get_object("ventanaimagen")
		self.ventana0.connect("destroy", Gtk.main_quit)
		self.ventana0.set_default_size(600, 400)
		self.ventana0.set_title("Informacion de la molecula: ")
		

		self.boton_visualizarM = self.builder.get_object("vizualizarmoleculas")
		self.boton_visualizarM.connect("clicked", self.VerMoleculas)
		
		self.textview_descripcion = self.builder.get_object("informacion")
		
		self.imagenmolecula = self.builder.get_object("imagen")
		
		self.boton_cancelar = self.builder.get_object("cancelar")
		self.boton_cancelar.connect("clicked", self.BotonCancelar)

		self.boton_aceptar = self.builder.get_object("aceptar")
		self.boton_aceptar.connect("clicked", self.BotonAceptar)
		
		self.ventana0.show_all()
		
	def BotonCancelar(self, btn=None):
		self.ventana0.destroy()
	def BotonAceptar(self, btn=None):
		self.ventana0.destroy()	
	def BotonDirectorio(self, btn=None):
		d = Ventanadirectorio()
	def VerMoleculas(self, btn=None):
		print(" por ahora nada")	


class Ventana1():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")

		self.ventana1= self.builder.get_object("ventana1")
		self.ventana1.connect("destroy", Gtk.main_quit)
		self.ventana1.set_default_size(600, 400)
		self.ventana1.set_title("Ventana Principal")

		self.boton_cargar= self.builder.get_object("cargarMolecula")
		self.boton_cargar.connect("clicked", self.CargarMolecula)
		

		self.boton_lista = self.builder.get_object("lista")
		self.boton_lista.connect("clicked", self.ListaMoleculas)
		self.ventana1.show_all()
	
	def ListaMoleculas(self, btn=None):
		d = Lista()
		self.Ventanalista.show_all()
	
	def importFile(self, *args):
		
		fn = gtk.FileChooserDialog(title="Import File",
								action=gtk.FILE_CHOOSER_ACTION_OPEN,
								buttons=
	(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
	
	
	
		self.ventana2.add_filter(_filter)
		_filter = Gtk.FileFilter()
		_filter.set_name("All Files")
		_filter.add_pattern("*")
		self.ventana2.add_filter(_filter)
		os.system('pymol *.mol2 -x -g ejemplo2.png -c -Q')
		os.system('pymol *.mol -x -g ejemplo2.png -c -Q')
		os.system('pymol *.pdb -x -g ejemplo2.png -c -Q')
		# ~ _filter = gtk.FileFilter()
		# ~ _filter.set_name("mol Files")
		# ~ _filter.add_pattern("*.mol" )
		# ~ _filter.add_pattern("*.mol2")
		# ~ _filter.add_pattern("*.pdb")
		
		fn.add_filter(_filter)
		_filter = gtk.FileFilter()
		_filter.set_name("All Files")
		_filter.add_pattern("*")
		fn.add_filter(_filter)
		fn.show()

		resp = fn.run()
		
		
		if resp == gtk.RESPONSE_OK:
			text = open(fn.get_filename()).read()

			self.addNotebookPage(os.path.basename(fn.get_filename()), text)
		fn.destroy()
		
	
	def CargarMolecula (self, btn=None):
		
		p = Ventanadirectorio()
		
		
	def ListaMoleculas(self, btn=None):
		d = Lista()
		
		
class Molecula():
	def __init__(self,directorio):
		
		self.nombre =  directorio
		self.imagenName = self.crearimagen()
		
	def crearimagen(self): 
		listDir = self.nombre.split('/')
		pdb_name =listDir[len(listDir)-1]
		print(pdb_name)
		pymol.finish_launching()
		pymol.cmd.load(self.nombre)
		pymol.cmd.mpng(pdb_name+'.png', 1,1)
		pymol.cmd.close_all()

		pymol.cmd.mpng("Ejemplo.png", width = 10 , height = 8, dpi = 300, ray=0)
		pymol.cmd.quit()
	
		
class Ventanadirectorio():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")

		self.ventana2= self.builder.get_object("ventanadirectorio")
		self.ventana2.connect("destroy", Gtk.main_quit)
		self.ventana2.set_default_size(600, 400)
		self.ventana2.set_title("Ventana Principal")

		self.boton_cancelar = self.builder.get_object("botoncancelar")
		self.boton_cancelar.connect("clicked", self.cancelar)

		self.boton_aceptar = self.builder.get_object("botonaceptar")
		self.boton_aceptar.connect("clicked", self.aceptar)
		
				
		_filter = Gtk.FileFilter()
		_filter.set_name("mol Files")
		_filter.add_pattern("*.pdb")
		_filter.add_pattern( "*.mol2")
		_filter.add_pattern("*.mol")
		self.ventana2.add_filter(_filter)
		_filter = Gtk.FileFilter()
		_filter.set_name("All Files")
		_filter.add_pattern("*")
		self.ventana2.add_filter(_filter)

		self.ventana2.show_all()

	def aceptar(self, btn=None):
		
		
		d = Imagen()
		
		print("self.nombre el boton add")
		#d = Lista()
		response = d.ventana2.run()
		directorio = self.ventana2.get_filename()
		self.ventana2.destroy()
		#newMolucula = Molecula(directorio)
		self.ventanadirectorio.destroy()
		

	

	
	def cancelar(self, btn=None):
		self.ventana2.destroy()
		
class  Lista():
	def __init__(self):
		
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")
		self.Ventanalista = self.builder.get_object("Lista1")
		self.Ventanalista.set_title("Lista de Moleculas")
		self.Ventanalista.set_default_size(600, 400)
			   
		self.listmodel = Gtk.ListStore(str, str, str,str)
		self.treeResultado = self.builder.get_object("ventanadirectorio")
		self.boton_ver = self.builder.get_object("botonvisualizar")
		self.boton_ver.connect("clicked", self.visualizar)
		cell = Gtk.CellRendererText()
		title = ("Nombre", "Estructura", "% elementos", "ruta")
		
		for i in range(len(title)):
			col = Gtk.TreeViewColumn(title[i], cell, text=i)
			# ~ self.treeResultado.append_column(col)
			self.show_all_data()
			self.Ventanalista.show_all()
			
	def visualizar(self, btn=None):
		print ("visualizar")

        
	def show_all_data(self):
		data = archivojson()
		for i in data:
			x = [x for x in i.values()]
			self.listmodel.append(x)

   		
if __name__ == "__main__":
	
	d = Ventana1()
	Gtk.main()
