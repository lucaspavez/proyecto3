# Proyecto3
Visualizador de moleculas:

Se diseño un software para poder visualizar proteinas  
Para esto fue necesario trabajar con pymol, archivos json y utilizando
la interfaz grafica de glade 

al iniciar el programa se debe escoger entre 2 opciones:

1) Cargar molecula: Si se presiona esta opción lo mandara al directorio 
a elegir una molecula para poder observar sus datos y tambien tendra la 
opcion de visualizarla

2) Lista de moleculas:Aqui se podran observar las moleculas
que ya han sido guardadas

Autor: Lucas Pavez Calleja

